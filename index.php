<?php
header('Content-Type: text/html; charset=UTF-8');

// метод текущего запроса $_SERVER['REQUEST_METHOD']
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    $messages[8] = '';
    $messages[9] = '';
    if (!empty($_COOKIE['save'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('save', '', 100000);
    
      // Если есть параметр save, то выводим сообщение пользователю.
      $messages[8] = '<div style="text-align: center; margin: 4px;">Спасибо, результаты сохранены.</div>';
      // Если в куках есть пароль, то выводим сообщение
       
      
    }


    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birthday'] = !empty($_COOKIE['birthday_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);

    $errors['abilities'] = !empty($_COOKIE['abilities_error']);
    $errors['contract'] = !empty($_COOKIE['contract_error']);

 
    if ($errors['fio']) {
      setcookie('fio_error', '', 100000);
      $messages[0] = '<div class="error_text">Заполните поле с именем!</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[1] = '<div class="error_text">Заполните почту!</div>';
    }
    if ($errors['birthday']) {
        setcookie('birthday_error', '', 100000);
        $messages[2] = '<div class="error_text">Выберите дату рождения!</div>';
    }
    if ($errors['gender']) {
      setcookie('gender_error', '', 100000);
      $messages[3] = '<div class="error_text">Выберите пол!</div>';
  }
  if ($errors['limbs']) {
    setcookie('limbs_error', '', 100000);
    $messages[4] = '<div class="error_text">Выберите количество конечностей!</div>';
  }
    if ($errors['biography']) {
      setcookie('biography_error', '', 100000);
      $messages[6] = '<div class="error_text">Заполните поле с биографией!</div>';
    }
    if ($errors['abilities']) {
      setcookie('abilities_error', '', 100000);
      $messages[5] = '<div class="error_text">Выберите способность!</div>';
    }
    if ($errors['contract']) {
      setcookie('contract_error', '', 100000);
      $messages[7] = '<div class="error_text">Вы не нажали галочку!</div>';
    }


    
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
      // TODO: загрузить данные пользователя из БД
      // и заполнить переменную $values,
      // предварительно санитизовав.
      $user = 'u24829';
      $pass = '5230726';
      $db = new PDO('mysql:host=localhost;dbname=u24829', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
      $stmt = $db->prepare("SELECT * FROM human_data WHERE id_human = ?");
      $stmt->execute([$_SESSION['uid']]);
      $row = $stmt ->fetch(PDO::FETCH_ASSOC);

      $values = array();
      $values['fio'] = $row["names"];
      $values['email'] = $row["e_mail"];
      $values['birthday'] = $row["hb"];
      $values['gender'] = $row["gend"];
      $values['limbs'] = $row["limbs"];
      $values['biography'] = $row["bio"];
      $stmt = $db->prepare("SELECT ab FROM app WHERE id_user = ?");
    }
    else {
      $values = array();
      $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
      $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
      $values['birthday'] = empty($_COOKIE['birthday_value']) ? '' : $_COOKIE['birthday_value'];
      $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
      $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
      $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
      $abilities = array();
      $abilities = empty($_COOKIE['abilities_values']) ? array() : unserialize($_COOKIE['abilities_values'], ["allowed_classes" => false]);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
  }

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('fio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (!preg_match('/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/', $_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
    else{
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['birthday'])) {
        setcookie('birthday_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        setcookie('birthday_value', $_POST['birthday'], time() + 30 * 24 * 60 * 60);
      }
    if (!isset($_POST['gender'])) {
      setcookie('gender_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['limbs'])) {
      setcookie('limbs_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['biography'])) {
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }

    if (!isset($_POST['abilities'])) {
      setcookie('abilities_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {

      setcookie('abilities_values', serialize($_POST['abilities']), time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['contract'])) {
      setcookie('contract_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }

  
  // Сохранить в Cookie признаки ошибок и значения полей.
// Тут необходимо проверить правильность заполнения всех остальных полей.

if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('birthday_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('contract_error', '', 100000);
  }

// Сохранение в базу данных.
 $user = 'u24829';
 $pass = '5230726';
 $db = new PDO('mysql:host=localhost;dbname=u24829', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

 if (!empty($_COOKIE[session_name()]) &&
     session_start() && !empty($_SESSION['login'])) {
   // TODO: перезаписать данные в БД новыми данными,
   // кроме логина и пароля.

   $stmt = $db->prepare("UPDATE human_data SET names = ?,e_mail = ?, hb = ?, gend = ? , limbs = ?, bio = ? WHERE id_human = ?");
   $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['birthday'],$_POST['gender'],$_POST['limbs'],$_POST['biography'],$_SESSION['uid']]);
   $stmt2 = $db->prepare("INSERT INTO app SET id_user= ?, ab = ?");

   foreach ($_POST['abilities'] as $s)
     $stmt2 -> execute([$_SESSION['uid'], $s]);
 }
 else {
   // Генерируем уникальный логин и пароль.
   // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
   $login = uniqid('',true);
   $pass = rand(10,100);
   // Сохраняем в Cookies.
   setcookie('login', $login);
   setcookie('pass', $pass);
   // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
   $stmt = $db->prepare("INSERT INTO human_data SET names = ?,e_mail = ?, hb = ?, gend = ? , limbs = ?, bio = ? ");
   $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['birthday'],$_POST['gender'],$_POST['limbs'],$_POST['biography']]);
   $stmt2 = $db->prepare("INSERT INTO app SET id_user= ?, ab = ?");
   $user_id = $db->lastInsertId();
   foreach ($_POST['abilities'] as $s)
     $stmt2 -> execute([$user_id, $s]);

   $stmt = $db->prepare("INSERT INTO login_n SET login_id = ?, pass = ?, user_id = ?");
   $stmt -> execute([$login,md5($pass),$user_id]);
 }

setcookie('save', '1');
header('Location: ?save=1');
}
