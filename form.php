<!DOCTYPE html>
<html lang="ru">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Задание 4</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
    <style>
/* Сообщения об ошибках */
.error {
  border: 2px solid red;
}
.error_text {
  margin: 4px;
  text-decoration-line: underline;
  text-decoration-color: red;
}
body{   text-align: center;
        background: #ffc2c6;
        font-family: Arial; 
        font-size: 15pt;} 
.h1{ font-size: 250%;}
    </style>
  </head>
<body>
      <main>
        <div id="sun" >
                <form action="" method="POST"><br/><br/>
                    <h1>Форма</h1><br/>
                <label>
                  Имя<br />
                  <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
                </label><br />
                 <?php if ($errors['fio']) {print $messages[0];} ?><br/>

                <label>
                  E-mail:<br />
                  <input name="email"
                  <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
                    type="email" />
                </label><br />
                <?php if ($errors['email']) {print $messages[1];} ?><br/>

                <label>
                  Дата рождения:<br />
                  <input name="birthday"
                    <?php if ($errors['birthday']) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>"
                    type="date" min="1940-01-01" max="2100-01-01">
                </label><br />
                <?php if ($errors['birthday']) {print $messages[2];} ?><br/>
                Пол:<br />
                <?php if ($errors['gender']) {print '<div class="error">';} ?>
                <label><input type="radio"
                  name="gender" value="Male" <?php if($values['gender']=='Male') print "checked";else print ""; ?> />
                  мужской
                </label>
                <label><input type="radio"
                  name="gender" value="Female" <?php echo ($values['gender']=='Female') ?  "checked" : "" ;  ?> />
                  женский
                </label>
                <br />
                <?php if ($errors['gender']) {print '</div>';print $messages[3];} ?><br/>


                Количество конечностей:<br />
                <?php if ($errors['limbs']) {print '<div class="error">';} ?>
                <label><input type="radio"
                  name="limbs" value="0" <?php echo ($values['limbs']=='0') ?  "checked" : "" ;  ?>/>
                  0
                </label>
                <label><input type="radio"
                  name="limbs" value="1" <?php echo ($values['limbs']=='1') ?  "checked" : "" ;  ?>/>
                  1
                </label>
                <label><input type="radio"
                  name="limbs" value="2" <?php echo ($values['limbs']=='2') ?  "checked" : "" ;  ?>/>
                  2
                </label>
                <label><input type="radio"
                  name="limbs" value="3" <?php echo ($values['limbs']=='3') ?  "checked" : "" ;  ?>/>
                  3
                </label>
                <label><input type="radio"
                  name="limbs" value="4" <?php echo ($values['limbs']=='4') ?  "checked" : "" ;  ?>/>
                  4
                </label>
                <label><input type="radio"
                  name="limbs" value="5" <?php echo ($values['limbs']=='5') ?  "checked" : "" ;  ?>/>
                  5
                </label>
                <br /><br/>
                  Сверхспособности:
                  <br />
                  <select name="abilities[]"
                    multiple="multiple" <?php if ($errors['abilities']) {print 'class="error"';} ?>>
                    <option value="0" <?php echo (in_array("0",$abilities)) ?  "selected" : ""  ; ?>>бессмертие</option>
                    <option value="2" <?php echo (in_array('2',$abilities)) ?  "selected" : ""  ; ?>>левитация</option>
                    <option value="1" <?php echo (in_array('1',$abilities)) ?  "selected" : ""  ; ?>>видеть будущее</option>
                  </select>
                <br /><br/>
                <?php if ($errors['abilities']) {print $messages[5];} ?>

                <label>
                  Биография:<br />
                  <textarea name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php echo $values['biography'] ;  ?></textarea>
                </label><br />
                <?php if ($errors['biography']) {print $messages[6];} ?>
                <br /><br/>
                <label><input type="checkbox"
                  name="contract" <?php if ($errors['contract']) {print 'class="error"';} ?>/>
                  Поставьте галочку
                </label><br /><br/>
                <?php if ($errors['contract']) {print $messages[7];} ?><br/>

                <input id="submit" type="submit" value="ok" />
              </form>
          </div>

          
      </main>
      <?php
  if (!empty($messages)) {
  print $messages[8];
  }
  ?>

</body>

</html>
